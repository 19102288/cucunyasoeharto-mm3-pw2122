<?php

namespace App\Http\Controllers;

use App\Espresso;
use Illuminate\Http\Request;

class EspressoController extends Controller
{
    public function create()
    {
        return view('admin.create-espresso');
    }
    public function index()
    {
        $espresso = Espresso::all();
        return view('admin.form-espresso', ['espresso' => $espresso]);
    }
    public function store(Request $request)
    {
        $validateData = $request -> validate([
            'image'  => 'required|file|image|max:10000',
            'jenis' => 'required|min:3|max:50',
            'nama'  => 'required|min:3|max:50',
            'harga' => 'required|min:3|max:50',
        ]);
        $espresso = new Espresso();
        if($request->hasFile('image')) 
        { 
            $extFile = $request->image->getClientOriginalExtension(); 
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->image->move('assets/images',$namaFile);
            $espresso->image = $path;
        }
        
        $espresso->jenis = $validateData['jenis'];
        $espresso->nama = $validateData['nama'];
        $espresso->harga = $validateData['harga'];
        $espresso->save();
        $request->session()->flash('pesan', 'data berhasil ditambahkan');
        return redirect()->route('admin.form');

    }
    public function edit($espresso) 
    { 
        $result = Espresso::findOrFail($espresso);
        return view('admin.edit-espresso',['espresso' => $result]); 
    
    }
    public function update($espresso, Request $request) 
    { 
        $validateData = $request->validate(
            [ 
                'image'  => 'required|file|image|max:10000',
                'jenis' => 'required|min:3|max:50',
                'nama'  => 'required|min:3|max:50',
                'harga' => 'required|min:3|max:50',
            ]);
            $espresso = Espresso::findOrFail($espresso);
            if($request->hasFile('image')){
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/images', $namaFile);
                $espresso->image = $path;
            } 
            $espresso->jenis = $validateData['jenis']; 
            $espresso->nama = $validateData['nama'];
            $espresso->harga = $validateData['harga'];
            $espresso->timestamps = false;
            $espresso->save();
            $request->session()->flash('pesan', 'Perubahan data berhasil');
            return redirect()->route('admin.form', ['espresso' => $espresso]);
    }
    public function destroy(Request $request, $espresso) 
    {
         $result = Espresso::find($espresso);
         $result->delete(); 
         $request->session()->flash('pesan','Hapus data berhasil'); 
         return redirect()->route('admin.form'); 
    }

    public function show(){
        $produk_espresso = Espresso::all();
        return view('produk.espresso', ['espresso' => $produk_espresso]);
    }
}

