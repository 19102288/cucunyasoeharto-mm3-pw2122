<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- title -->
	<title>Baristavro</title>

	<!-- favicon -->
	<link rel="shortcut icon" type="image/png" href="assets/img/logoBaristavro.svg">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
	<!-- fontawesome -->
	<link rel="stylesheet" href="assets/css/all.min.css">
	<!-- bootstrap -->
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
	<!-- owl carousel -->
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<!-- magnific popup -->
	<link rel="stylesheet" href="assets/css/magnific-popup.css">
	<!-- animate css -->
	<link rel="stylesheet" href="assets/css/animate.css">
	<!-- mean menu css -->
	<link rel="stylesheet" href="assets/css/meanmenu.min.css">
	<!-- main style -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- responsive -->
	<link rel="stylesheet" href="assets/css/responsive.css">

</head>
<body>
	
	<!--PreLoader-->
    <div class="loader">
        <div class="loader-inner">
            <div class="circle"></div>
        </div>
    </div>
    <!--PreLoader Ends-->
	
	<!-- header -->
	<div class="top-header-area" id="sticker">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-sm-12 text-center">
					<div class="main-menu-wrap">
						<!-- logo -->
						<div class="site-logo">
							<a href="{{ route ('index.index') }}">
								<img src="assets/img/logo.png" alt="">
							</a>
						</div>
						<!-- logo -->

						<!-- menu start -->
						<nav class="main-menu">
							<ul>
								<li class="current-list-item"><a href="#">Home</a></li>
								<li><a href="{{ route ('produk.espressoShow') }}">Machine Espresso</a>
								</li>
								<li><a href="{{ route ('produk.grindersShow') }}">Grinders</a>
									
								</li>
								
								<li><a href="{{ route ('produk.beansShow') }}">Coffee Beans</a>
									
								</li>
								<li><a href=" {{ route ('pesan.pesan') }}">Contact</a></li>
								<li>
									<div class="header-icons">
											<a href="{{url ('/login') }}" class="boxed-btn">Admin</a>
										</div>
									</div>
								</li>
							</ul>
						</nav>
						<a class="mobile-show search-bar-icon" href="#"><i class="fas fa-search"></i></a>
						<div class="mobile-menu"></div>
						<!-- menu end -->
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end header -->
	

	<!-- hero area -->
	<div class="hero-area hero-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 offset-lg-2 text-center">
					<div class="hero-text">
						<div class="hero-text-tablecell">
							<img src="assets/img/secangkir.png" alt="">
							<p class="subtitle">Shopping Now 	</p>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end hero area -->

	<!-- features list section -->
	<div class="list-section pt-80 pb-80">
		<div class="container">

			<div class="row">
				<div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
					<div class="list-box d-flex align-items-center">
						<div class="list-icon">
							<i class="fas fa-shipping-fast"></i>
						</div>
						<div class="content">
							<h3>Free Shipping</h3>
							<p>When order over $75</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
					<div class="list-box d-flex align-items-center">
						<div class="list-icon">
							<i class="fas fa-phone-volume"></i>
						</div>
						<div class="content">
							<h3>24/7 Support</h3>
							<p>Get support all day</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="list-box d-flex justify-content-start align-items-center">
						<div class="list-icon">
							<i class="fas fa-sync"></i>
						</div>
						<div class="content">
							<h3>Refund</h3>
							<p>Get refund within 3 days!</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- end features list section -->
	
	<!-- product section -->
	<div class="product-section mt-150 mb-150">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center">
					<div class="section-title">	
						<h3><span class="orange-text">Our</span> Products</h3>
						<p>We provide a product / tool to make the coffee you want, from the best espresso machines, grinders to coffee beans.</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-4 col-md-6 text-center">
					<div class="single-product-item">
						<div class="product-image">
							<a href="single-product.html"><img src="assets/img/products/kopi 2.png" alt=""></a>
						</div>
						<h3>Cunnil Brasil Coffee Grinders</h3>
						<p class="product-price"><span>Grinders</span>Rp1.000.000  </p>
						<a type="button" class="cart-btn"
                        href="https://wa.me/+6285383867028?text=Hai%2C%20Apakah%20barang%20tersedia%20?"
                        target="_blank">Pesan sekarang</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 text-center">
					<div class="single-product-item">
						<div class="product-image">
							<a href="single-product.html"><img src="assets/img/products/la2.png" alt=""></a>
						</div>
						<h3>La Marzocco GS3 MP</h3>
						<p class="product-price"><span>Espresso machine</span> Rp2.000.000 </p>
						<a type="button" class="cart-btn"
                        href="https://wa.me/+6285383867028?text=Hai%2C%20Apakah%20barang%20tersedia%20?"
                        target="_blank">Pesan sekarang</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 offset-md-3 offset-lg-0 text-center">
					<div class="single-product-item">
						<div class="product-image">
							<a href="single-product.html"><img src="assets/img/products/cb3.jpg" alt=""></a>
						</div>
						<h3>Coffe redo Tompaso Minahasa</h3>
						<p class="product-price"><span>coffe beans</span>Rp70.000 </p>
						<a type="button" class="cart-btn"
                        href="https://wa.me/+6285383867028?text=Hai%2C%20Apakah%20barang%20tersedia%20?"
                        target="_blank">Pesan sekarang</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end product section -->


	<!-- logo carousel -->
	<div class="logo-carousel-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="logo-carousel-inner">
						<div class="single-logo-item">
							<img src="assets/img/company-logos/La_Marzocco.png" alt="">
						</div>
						<div class="single-logo-item">
							<img src="assets/img/company-logos/ferati.png" alt="">
						</div>
						<div class="single-logo-item">
							<img src="assets/img/company-logos/otten.png" alt="">
						</div>
						<div class="single-logo-item">
							<img src="assets/img/company-logos/aeropress.png" alt="">
						</div>
						<div class="single-logo-item">
							<img src="assets/img/company-logos/harrio.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end logo carousel -->

	<!-- find our location -->
	<div class="find-location blue-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<p> <i class="fas fa-map-marker-alt"></i> Find Our Location</p>
				</div>
			</div>
		</div>
	</div>

	<!-- end find our location -->
	<!-- google map section -->
	<div class="embed-responsive embed-responsive-21by9">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.2734922787863!2d109.2471338147759!3d-7.434959494636067!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e655ea49d9f9885%3A0x62be0b6159700ec9!2sInstitut%20Teknologi%20Telkom%20Purwokerto!5e0!3m2!1sid!2sid!4v1643024766099!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
	</div>
	<!-- end google map section -->
	
	<!-- footer -->
	<div class="footer-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="footer-box about-widget">
						<h2 class="widget-title">About us</h2>
						<p>Baristavro is a website aimed at coffee lovers, baristavro also sells a variety of tools and coffee beans to help you in perfecting your coffee worship.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="footer-box pages">
						<h2 class="widget-title">Contact Us</h2>
						<ul>
							<li>Email</li>
							<li>Call us</li>
							<li>Chat online</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="footer-box get-in-touch">
						<h2 class="widget-title">Policy</h2>
						<ul>
							<li>Shipping</li>
							<li>Returns and Refund</li>
							<li>Privacy Policy</a></li>
							<li>Cookie Policy</li>
						</ul>
					</div>
				</div>
				<!-- logo -->
				<div class="col-lg-3 col-md-6">
					<div class="footer-box get-in-touch"
					<a href="index.html">
						<img src="assets/img/logo.png" alt="">
					</a>
				</div>
				<!-- logo -->
			</div>
		</div>
	</div>
	<!-- end footer -->
	
	<!-- copyright -->
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-12">
					<p>Copyrights &copy; 2022 - <a href="index.html">Baristavro</a>,  All Rights Reserved.</p>
				</div>
				<div class="col-lg-6 text-right col-md-12">
					<div class="social-icons">
						<ul>
							<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end copyright -->
	
	<!-- jquery -->
	<script src="{{ url ('') }}/assets/js/jquery-1.11.3.min.js"></script>
	<!-- bootstrap -->
	<script src="{{ url ('') }}/assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- count down -->
	<script src="{{ url ('') }}/assets/js/jquery.countdown.js"></script>
	<!-- isotope -->
	<script src="{{ url ('') }}/assets/js/jquery.isotope-3.0.6.min.js"></script>
	<!-- waypoints -->
	<script src="{{ url ('') }}/assets/js/waypoints.js"></script>
	<!-- owl carousel -->
	<script src="{{ url ('') }}/assets/js/owl.carousel.min.js"></script>
	<!-- magnific popup -->
	<script src="{{ url ('') }}/assets/js/jquery.magnific-popup.min.js"></script>
	<!-- mean menu -->
	<script src="{{ url ('') }}/assets/js/jquery.meanmenu.min.js"></script>
	<!-- sticker js -->
	<script src="{{ url ('') }}/assets/js/sticker.js"></script>
	<!-- main js -->
	<script src="{{ url ('') }}/assets/js/main.js"></script>

</body>
</html>