<?php

namespace App\Http\Controllers;

use App\Pesan;
use Illuminate\Http\Request;

class PesanController extends Controller
{

    public function welcome(){
        return view('pesan.pesan');
    }
    public function create()
    {
        return view('pesan.create');
    }
    public function index()
    {
        $pesan = Pesan::all();
        return view('pesan.form-pesan', ['pesan' => $pesan]);
    }
    public function store(Request $request) 
    { 
        $validateData = $request->validate
        ([  
            'nama' => 'required|min:3|max:50', 
            'email' => 'required', 
            'phone' => 'required',
            'subject' => 'required',
            'pesan' => '', 
        ]); 
        $pesan = new Pesan(); 
        $pesan->nama = $validateData['nama']; 
        $pesan->email = $validateData['email'];
        $pesan->phone = $validateData['phone'];
        $pesan->subject = $validateData['subject'];
        $pesan->pesan = $validateData['pesan'];
        $pesan->timestamps = false;
        $pesan->save();
        $request->session()->flash('pesan','Pesan berhasil dikirm');
        return redirect()->route('pesan.pesan');
    }
}
