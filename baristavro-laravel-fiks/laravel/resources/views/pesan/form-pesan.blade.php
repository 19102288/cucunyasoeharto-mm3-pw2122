<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard Baristavro</title>
	<!-- favicon -->
	<link rel="shortcut icon" type="image/png" href="assets/img/logoBaristavro.svg">
    <!-- Custom fonts for this template-->
    <link href="{{ url ('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    
    <!-- Custom styles for this template-->
    <link href="{{ url ('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/dashboard') }}">
                <div class="sidebar-brand-text mx-3"> Admin Baristavro </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/dashboard') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Database
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">

                <!-- Nav Item - Machine Espresso -->
            <li class="nav-item">
                <a class="nav-link" href="{{ url('form-espresso') }}">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Machine Espresso</span></a>
            </li>

            <!-- Nav Item - Grinders -->
            <li class="nav-item">
                <a class="nav-link" href="{{ url('form-grinders') }}">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Grinders</span></a>
            </li>

            <!-- Nav Item - Coffe beans -->
            <li class="nav-item">
                <a class="nav-link" href="{{ url('form-beans') }}">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Coffe Beans</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Pesan User</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
            
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/logout') }}" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-coffee"></i>
                    <span>Logout</span></a>
            </li>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>


                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">admin/<b>pesan</b>
                            <h1>
                    </div>

                    <h5>Pesan dari user baristavro</h5>
                    @if (session() -> has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan')}}
                    </div>
                    @endif


                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th style="text-align: center;" scope="col">nama</th>
                                <th style="text-align: center;" scope="col">email</th>
                                <th style="text-align: center;" scope="col">phone</th>
                                <th style="text-align: center;" scope="col">subject</th>
                                <th style="text-align: center;" scope="col">pesan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($pesan as $pesan)
                            <tr>
                                <th>{{$loop->iteration}}</th>
                                <td align="center">{{$pesan->nama}}</td>
                                <td align="center">{{$pesan->email}}</td>
                                <td align="center">{{$pesan->phone}}</td>
                                <td align="center">{{$pesan->subject}}</td>
                                <td align="center">{{$pesan->pesan}}</td>

                            @empty
                            <td colspan="6" class="text-center"> belum ada data </td>
                            @endforelse
                        </tbody>
                    </table>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Baristavro 2022</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="{{route ('index.index') }}">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ url ('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url ('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ url ('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="/{{ url ('assets/js/sb-admin-2.min.js') }}"></script>

    <!-- Page level plugins -->
    <script src="{{ url ('assets/vendor/chart.js/Chart.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ url ('assets/js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ url ('assets/js/demo/chart-pie-demo.js') }}"></script>

</body>

</html>