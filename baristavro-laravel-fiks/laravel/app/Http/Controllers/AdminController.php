<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Admin;

class AdminController extends Controller
{
    public function index()
    {
        return view('login.login');
    }
    public function process(Request$request)
    {
        $validateData = $request->validate
        ([
            'username'=>'required',
            'password'=>'required',
        ]);
            $result=Admin::where('username','=',$validateData['username'])->first();
            if($result){
                if (($request->password == $result->password))
                {
                    session(['username'=>$request->username]);
                    return redirect('dashboard');
                }
                else {
                    return back()->withInput()->with('pesan',"Login Gagal");
                }
            }else
            {
                return back()->withInput()->with('pesan',"Login Gagal");
            }
        }
        public function logout()
        {
            session()->forget('username');
            return redirect('login')->with('pesan','Logout berhasil');
        }

        public function store(Request $request)
    {
        $validateData = $request -> validate([
            'username'  => 'required|min:3|max:50',
            'password' => 'required|min:3|max:50',
        ]);
        
        $admin = new Admin();
        $admin->username = $validateData['username'];
        $admin->password = $validateData['password'];
        $admin->save();
        $request->session()->flash('pesan', 'admin baru ditambahkan');
        return redirect()->route('login.login');

    }
    public function registrasi()
    {
        return view('login.registrasi');
    }
}
