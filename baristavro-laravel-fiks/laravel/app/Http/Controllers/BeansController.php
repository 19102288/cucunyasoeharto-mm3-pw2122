<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Beans;

class BeansController extends Controller
{
    public function index()
    {
        $beans = Beans::all();
        return view('admin.form-beans', ['beans' => $beans]);
    }

    public function create()
    {
        return view('admin.create-beans');
    }

    public function store(Request $request)
    {
        $validateData = $request -> validate([
            'image'  => 'required|file|image|max:10000',
            'jenis' => 'required|min:3|max:50',
            'nama'  => 'required|min:3|max:50',
            'harga' => 'required|min:3|max:50',
        ]);
        $beans = new Beans();
        if($request->hasFile('image')) 
        { 
            $extFile = $request->image->getClientOriginalExtension(); 
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->image->move('assets/images',$namaFile);
            $beans->image = $path;
        }
        $beans->jenis = $validateData['jenis'];
        $beans->nama = $validateData['nama'];
        $beans->harga = $validateData['harga'];
        $beans->save();
        $request->session()->flash('pesan', 'data berhasil ditambahkan');
        return redirect()->route('admin.form-beans');
    }

    public function edit($beans) 
    { 
        $result = Beans::findOrFail($beans);
        return view('admin.edit-beans',['beans' => $result]); 
    
    }

    public function update($beans, Request $request) 
    { 
        $validateData = $request->validate(
            [ 
                'image'  => 'required|file|image|max:10000',
                'jenis' => 'required|min:3|max:50',
                'nama'  => 'required|min:3|max:50',
                'harga' => 'required|min:3|max:50',
            ]);
            $beans = Beans::findOrFail($beans);
            if($request->hasFile('image')){
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/images', $namaFile);
                $beans->image = $path;
            } 
            $beans->jenis = $validateData['jenis']; 
            $beans->nama = $validateData['nama'];
            $beans->harga = $validateData['harga'];
            $beans->timestamps = false;
            $beans->save();
            $request->session()->flash('pesan', 'Perubahan data berhasil');
            return redirect()->route('admin.form-beans', ['espresso' => $beans]);
    }
    public function destroy(Request $request, $beans) 
    {
         $result = Beans::find($beans);
         $result->delete(); 
         $request->session()->flash('pesan','Hapus data berhasil'); 
         return redirect()->route('admin.form-beans'); 
    }
    
    public function show(){
        $produk_beans = Beans::all();
        return view('produk.beans', ['beans' => $produk_beans]);
    }
}
