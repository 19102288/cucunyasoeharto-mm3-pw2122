<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login','AdminController@index')->name('login.login');
Route::get('/logout','AdminController@logout')->name('login.logout');
Route::post('/login','AdminController@process')->name('login.process');

Route::get('/dashboard', function(){
    return view('admin.dashboard');
});