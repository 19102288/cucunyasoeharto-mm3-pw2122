<?php

use App\Beans;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', 'IndexController@show') ->name('index.index');

Route::get('/login','AdminController@index')->name('login.login');
Route::get('/registrasi', 'AdminController@registrasi')->name('login.registrasi');
Route::get('/logout','AdminController@logout')->name('login.logout');
Route::post('/login','AdminController@process')->name('login.process');
Route::post('/registrasi','AdminController@store')->name('login.registrasi');

Route::get('/form-pesan', 'PesanController@index') ->name('pesan.form');
Route::get('/pesan', 'PesanController@welcome')->name('pesan.pesan');
Route::get('/pesan-create', 'PesanController@create')->name('pesan.create');
Route::post('/pesan-post', 'PesanController@store') ->name('pesan.storePesan');

Route::get('/beans', 'BeansController@show')->name('produk.beansShow');
Route::get('/espresso', 'EspressoController@show')->name('produk.espressoShow');
Route::get('/grinders', 'GrindersController@show')->name('produk.grindersShow');

Route::get('/dashboard', function(){
    return view('admin.dashboard');
})->middleware('login_auth');

Route::get('/form-espresso', 'EspressoController@index') ->name('admin.form')->middleware('login_auth');
Route::get('/create-espresso', 'EspressoController@create')->name('admin.create')->middleware('login_auth');
Route::post('/post-espresso', 'EspressoController@store')->name('admin.storeEspresso')->middleware('login_auth');
Route::get('/adminEspresso/{id}/edit-espresso', 'EspressoController@edit') ->name('admin.edit-espresso')->middleware('login_auth');
Route::patch('/adminEspresso/{id}', 'EspressoController@update') ->name('admin.updateEspresso')->middleware('login_auth');
Route::delete('/adminEspresso/{id}', 'EspressoController@destroy') ->name('admin.destroyEspresso')->middleware('login_auth');

Route::get('/form-grinders', 'GrindersController@index') ->name('admin.form-grinders')->middleware('login_auth');
Route::get('/create-grinders', 'GrindersController@create')->name('admin.createGrinders')->middleware('login_auth');
Route::post('/post-grinders', 'GrindersController@store')->name('admin.storeGrinders')->middleware('login_auth');
Route::get('/adminGrinders/{id}/edit-grinders', 'GrindersController@edit') ->name('admin.edit-grinders')->middleware('login_auth');
Route::patch('/adminGrinders/{id}', 'GrindersController@update') ->name('admin.updateGrinders')->middleware('login_auth');
Route::delete('/adminGrinders/{id}', 'GrindersController@destroy') ->name('admin.destroyGrinders')->middleware('login_auth');

Route::get('/form-beans', 'BeansController@index') ->name('admin.form-beans')->middleware('login_auth');
Route::get('/create-beans', 'BeansController@create')->name('admin.createBeans')->middleware('login_auth');
Route::post('/post-beans', 'BeansController@store')->name('admin.storeBeans')->middleware('login_auth');
Route::get('/adminBeans/{id}/edit-beans', 'BeansController@edit') ->name('admin.edit-beans')->middleware('login_auth');
Route::patch('/adminBeans/{id}', 'BeansController@update') ->name('admin.updateBeans')->middleware('login_auth');
Route::delete('/adminBeans/{id}', 'BeansController@destroy') ->name('admin.destroyBeans')->middleware('login_auth');