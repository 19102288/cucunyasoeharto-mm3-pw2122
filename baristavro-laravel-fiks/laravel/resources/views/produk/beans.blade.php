<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Responsive Bootstrap4 Shop Template, Created by Imran Hossain from https://imransdesign.com/">

	<!-- title -->
	<title>Coffe Beans</title>

	<!-- favicon -->
	<link rel="shortcut icon" type="image/png" href="assets/img/logoBaristavro.svg">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
	<!-- fontawesome -->
	<link rel="stylesheet" href="{{url ('assets/css/all.min.css') }} /assets/css/all.min.css">
	<!-- bootstrap -->
	<link rel="stylesheet" href="{{url ('assets/bootstrap/css/bootstrap.min.css') }} ">
	<!-- owl carousel -->
	<link rel="stylesheet" href="{{url ('assets/css/owl.carousel.css') }} ">
	<!-- magnific popup -->
	<link rel="stylesheet" href="{{url ('assets/css/magnific-popup.css') }} ">
	<!-- animate css -->
	<link rel="stylesheet" href="{{url ('assets/css/animate.css') }} ">
	<!-- mean menu css -->
	<link rel="stylesheet" href="{{url ('assets/css/meanmenu.min.css') }} ">
	<!-- main style -->
	<link rel="stylesheet" href="{{url ('assets/css/main.css') }} ">
	<!-- responsive -->
	<link rel="stylesheet" href="{{url ('assets/css/responsive.css') }} ">

</head>

<body>

	<!--PreLoader-->
	<!-- <div class="loader">
		<div class="loader-inner">
			<div class="circle"></div>
		</div>
	</div> -->
	<!--PreLoader Ends-->

	<!-- header -->
	<div class="top-header-area" id="sticker">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-sm-12 text-center">
					<div class="main-menu-wrap">
						<!-- logo -->
						<div class="site-logo">
							<a href="{{ route ('index.index') }}">
								<img src="assets/img/logo.png" alt="">
							</a>
						</div>
						<!-- logo -->

						<!-- menu start -->
						<nav class="main-menu">
							<ul>
								<li><a href="{{ route('index.index') }}">Home</a></li>
								<li><a href="{{ route('produk.espressoShow') }}">Machine Espresso</a>

								</li>
								<li><a href="{{ route('produk.grindersShow') }}">Grinders</a>

								</li>

								<li class="current-list-item"><a href="#">Coffe Beans</a>
								</li>
								<li><a href=" {{ route ('pesan.pesan') }}">Contact</a></li>
								<li>
									<div class="header-icons">
										<a href="{{url ('/login') }}" class="boxed-btn">Admin</a>
									</div>
					</div>
					</li>
					</ul>
					</nav>
					<a class="mobile-show search-bar-icon" href="#"><i class="fas fa-search"></i></a>
					<div class="mobile-menu"></div>
					<!-- menu end -->
				</div>
			</div>
		</div>
	</div>
	</div>
	<!-- end header -->


	<!-- breadcrumb-section -->
	<div class="breadcrumb-section breadcrumb-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center">
					<div class="breadcrumb-text">
						<p> Cheap and Trifthy</p>
						<h1>Coffe Beans</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end breadcrumb section -->

	<!-- products -->
	<div class="product-section mt-150 mb-150">
		<div class="container">

			<div class="row">
				<div class="col-md-12">
					<div class="product-filters">
						<ul>
							<li class="active" data-filter="*">All Coffe beans</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="row">
				@forelse ($beans as $produk_beans)
				<div class="col-lg-4 col-md-6 text-center">
					<div class="single-product-item">
						<div class="product-image">
							<img src="{{ url('') }}/{{$produk_beans->image}}" alt=""></a>
						</div>
						<h3>{{$produk_beans->nama}}</h3>
						<p class="product-price"><span>{{$produk_beans->jenis}}</span> {{$produk_beans->harga}} </p>
						<a href="https://wa.me/+6285383867028?text=Hai%2C%20Apakah%20barang%20tersedia%20?" class="cart-btn"><i class="fas fa-shopping-cart"></i> Pesan Sekarang</a>
					</div>
				</div>
				@empty
				<h4 style="text-align:center; color:white;">Produk Tidak ada</h4>
				@endforelse
			</div>
		</div>
	</div>
	<!-- end products -->

	<!-- logo carousel -->
	<div class="logo-carousel-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="logo-carousel-inner">
						<div class="single-logo-item">
							<img src="assets/img/company-logos/La_Marzocco.png" alt="">
						</div>
						<div class="single-logo-item">
							<img src="assets/img/company-logos/ferati.png" alt="">
						</div>
						<div class="single-logo-item">
							<img src="assets/img/company-logos/otten.png" alt="">
						</div>
						<div class="single-logo-item">
							<img src="assets/img/company-logos/aeropress.png" alt="">
						</div>
						<div class="single-logo-item">
							<img src="assets/img/company-logos/harrio.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end logo carousel -->

	<!-- footer -->
	<div class="footer-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="footer-box about-widget">
						<h2 class="widget-title">About us</h2>
						<p>Baristavro is a website aimed at coffee lovers, baristavro also sells a variety of tools and coffee beans to help you in perfecting your coffee worship.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="footer-box pages">
						<h2 class="widget-title">Contact Us</h2>
						<ul>
							<li>Email</li>
							<li>Call us</li>
							<li>Chat online</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="footer-box get-in-touch">
						<h2 class="widget-title">Policy</h2>
						<ul>
							<li>Shipping</li>
							<li>Returns and Refund</li>
							<li>Privacy Policy</a></li>
							<li>Cookie Policy</li>
						</ul>
					</div>
				</div>
				<!-- logo -->
				<div class="col-lg-3 col-md-6">
					<div class="footer-box get-in-touch" <a href="{{route ('index.index') }}">
						<img src="assets/img/logo.png" alt="">
						</a>
					</div>
					<!-- logo -->
				</div>
			</div>
		</div>
		<!-- end footer -->

		<!-- copyright -->
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-12">
						<p>Copyrights &copy; 2019 - <a href="index.html">Baristavro</a>, All Rights Reserved.</p>
					</div>
					<div class="col-lg-6 text-right col-md-12">
						<div class="social-icons">
							<ul>
								<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end copyright -->

		<!-- jquery -->
		<script src="{{url('') }} /assets/js/jquery-1.11.3.min.js"></script>
		<!-- bootstrap -->
		<script src="{{url('') }} /assets/bootstrap/js/bootstrap.min.js"></script>
		<!-- count down -->
		<script src="{{url('') }} /assets/js/jquery.countdown.js"></script>
		<!-- isotope -->
		<script src="{{url('') }} /assets/js/jquery.isotope-3.0.6.min.js"></script>
		<!-- waypoints -->
		<script src="{{url('') }} /assets/js/waypoints.js"></script>
		<!-- owl carousel -->
		<script src="{{url('') }} /assets/js/owl.carousel.min.js"></script>
		<!-- magnific popup -->
		<script src="{{url('') }} /assets/js/jquery.magnific-popup.min.js"></script>
		<!-- mean menu -->
		<script src="{{url('') }} /assets/js/jquery.meanmenu.min.js"></script>
		<!-- sticker js -->
		<script src="{{url('') }} /assets/js/sticker.js"></script>
		<!-- main js -->
		<script src="{{url('') }} /assets/js/main.js"></script>

</body>

</html>