<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grinders;

class GrindersController extends Controller
{
    public function create()
    {
        return view('admin.create-Grinders');
    }
    public function index()
    {
        $grinders = Grinders::all();
        return view('admin.form-grinders', ['grinders' => $grinders]);
    }
    public function store(Request $request)
    {
        $validateData = $request -> validate([
            'image'  => 'required|file|image|max:10000',
            'jenis' => 'required|min:3|max:50',
            'nama'  => 'required|min:3|max:50',
            'harga' => 'required|min:3|max:50',
        ]);
        $grinders = new Grinders();
        if($request->hasFile('image')) 
        { 
            $extFile = $request->image->getClientOriginalExtension(); 
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->image->move('assets/images',$namaFile);
            $grinders->image = $path;
        }
        $grinders->jenis = $validateData['jenis'];
        $grinders->nama = $validateData['nama'];
        $grinders->harga = $validateData['harga'];
        $grinders->save();
        $request->session()->flash('pesan', 'data berhasil ditambahkan');
        return redirect()->route('admin.form-grinders');

    }
    public function edit($grinders) 
    { 
        $result = Grinders::findOrFail($grinders);
        return view('admin.edit-grinders',['grinders' => $result]); 
    
    }
    public function update($grinders, Request $request) 
    { 
        $validateData = $request->validate(
            [ 
                'image'  => 'required|file|image|max:10000',
                'jenis' => 'required|min:3|max:50',
                'nama'  => 'required|min:3|max:50',
                'harga' => 'required|min:3|max:50',
            ]);
            $grinders = Grinders::findOrFail($grinders);
            if($request->hasFile('image')){
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/images', $namaFile);
                $grinders->image = $path;
            } 
            $grinders->jenis = $validateData['jenis']; 
            $grinders->nama = $validateData['nama'];
            $grinders->harga = $validateData['harga'];
            $grinders->timestamps = false;
            $grinders->save();
            $request->session()->flash('pesan', 'Perubahan data berhasil');
            return redirect()->route('admin.form-grinders', ['espresso' => $grinders]);
    }
    public function destroy(Request $request, $grinders) 
    {
         $result = Grinders::find($grinders);
         $result->delete(); 
         $request->session()->flash('pesan','data berhasil dihapus'); 
         return redirect()->route('admin.form-grinders'); 
    }

    public function show(){
        $produk_grinders = Grinders::all();
        return view('produk.grinders', ['grinders' => $produk_grinders]);
    }
}
