<?php

namespace App\Http\Controllers;

use App\Grinders;
use Illuminate\Http\Request;
use App\Beans;
use App\Espresso;

class IndexController extends Controller
{
    public function show(){
        $beans = Beans::all();
        $grinders = Grinders::all();
        $espresso = Espresso::all();
        return view('index.index', ['beans' => $beans, 'grinders' => $grinders, 'espresso' => $espresso]);
    }
}
