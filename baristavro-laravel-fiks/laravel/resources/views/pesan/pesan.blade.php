<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Responsive Bootstrap4 Shop Template, Created by Imran Hossain from https://imransdesign.com/">

	<!-- title -->
	<title>Contact</title>

	<!-- favicon -->
	<link rel="shortcut icon" type="image/png" href="assets/img/logoBaristavro.svg">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
	<!-- fontawesome -->
	<link rel="stylesheet" href="{{url ('assets/css/all.min.css') }} /assets/css/all.min.css">
	<!-- bootstrap -->
	<link rel="stylesheet" href="{{url ('assets/bootstrap/css/bootstrap.min.css') }} ">
	<!-- owl carousel -->
	<link rel="stylesheet" href="{{url ('assets/css/owl.carousel.css') }} ">
	<!-- magnific popup -->
	<link rel="stylesheet" href="{{url ('assets/css/magnific-popup.css') }} ">
	<!-- animate css -->
	<link rel="stylesheet" href="{{url ('assets/css/animate.css') }} ">
	<!-- mean menu css -->
	<link rel="stylesheet" href="{{url ('assets/css/meanmenu.min.css') }} ">
	<!-- main style -->
	<link rel="stylesheet" href="{{url ('assets/css/main.css') }} ">
	<!-- responsive -->
	<link rel="stylesheet" href="{{url ('assets/css/responsive.css') }} ">

</head>
<body>
	
	<!--PreLoader-->
    <!-- <div class="loader">
        <div class="loader-inner">
            <div class="circle"></div>
        </div>
    </div> -->
    <!--PreLoader Ends-->
	
	<!-- header -->
	<!-- header -->
	<div class="top-header-area" id="sticker">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-sm-12 text-center">
					<div class="main-menu-wrap">
						<!-- logo -->
						<div class="site-logo">
							<a href="{{ route ('index.index') }}">
								<img src="assets/img/logo.png" alt="">
							</a>
						</div>
						<!-- logo -->

						<!-- menu start -->
						<nav class="main-menu">
							<ul>
								<li><a href="{{ route('index.index') }}">Home</a></li>
								<li><a href="{{ route('produk.espressoShow') }}">Machine Espresso</a>

								</li>
								<li><a href="{{ route('produk.grindersShow') }}">Grinders</a>

								</li>

								<li><a href="{{ route('produk.beansShow') }}">Coffe Beans</a>
								</li>
								<li class="current-list-item"><a href="contact.html">Contact</a></li>
								<li>
									<div class="header-icons">
										<a href="{{url ('/login') }}" class="boxed-btn">Admin</a>
									</div>
					</div>
					</li>
					</ul>
					</nav>
					<a class="mobile-show search-bar-icon" href="#"><i class="fas fa-search"></i></a>
					<div class="mobile-menu"></div>
					<!-- menu end -->
				</div>
			</div>
		</div>
	</div>
	<!-- end header -->

	
	<!-- breadcrumb-section -->
	<div class="breadcrumb-section breadcrumb-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center">
					<div class="breadcrumb-text">
						<p>Get 24/7 Support</p>
						<h1>Contact us</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end breadcrumb section -->

	<!-- contact form -->
	<div class="contact-from-section mt-150 mb-150">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mb-5 mb-lg-0">
					<div class="form-title">
						<h2>Have you any question?</h2>
						<p>Pesan mu akan terbaca oleh admin dan akan dibalas jika memungkinkan</p>
					</div>
				 	<div id="form_status"></div>
					<div class="contact-form">
						<form action="{{route('pesan.storePesan')}}" method="POST">
                            <p>
                                @csrf
								<input type="text" class="@error('nama') is-invalid @enderror" placeholder="nama" name="nama" id="nama" value="{{ old('nama') }}">
                                @error('nama')
                                    <div class="text-danger"> {{ $message }}</div>
                                @enderror
								<input type="text" class="@error('email') is-invalid @enderror" placeholder="Email" name="email" id="email" value="{{ old('email') }}">
                                @error('email')
                                    <div class="text-danger"> {{ $message }}</div>
                                @enderror
							</p>
                            
							<p>
								<input type="text" class="@error('phone') is-invalid @enderror" placeholder="Phone" name="phone" id="phone" value="{{ old('phone') }}">
                                @error('phone')
                                    <div class="text-danger"> {{ $message }}</div>
                                @enderror
								<input type="text" class="@error('subject') is-invalid @enderror" placeholder="Subject" name="subject" id="subject" value="{{ old('subject') }}">
                                @error('subject')
                                    <div class="text-danger"> {{ $message }}</div>
                                @enderror
							</p>

							<p><textarea name="pesan" id="pesan" cols="30" rows="10" placeholder="Pesan">{{old ('pesan') }}</textarea></p>
							<button type="submit" class="btn btn-primary mb-2">Submit</button>
						</form>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="contact-form-wrap">
						<div class="contact-form-box">
							<h4><i class="fas fa-map"></i> Shop Address</h4>
							<p>telkom <br> banyumas<br> indonesia</p>
						</div>
						<div class="contact-form-box">
							<h4><i class="far fa-clock"></i> Shop Hours</h4>
							<p>MON - FRIDAY: 8 to 9 PM <br> SAT - SUN: 10 to 8 PM </p>
						</div>
						<div class="contact-form-box">
							<h4><i class="fas fa-address-book"></i> Contact</h4>
							<p>Phone: +62 111 222 3333 <br> Email: support@baristavro.com</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end contact form -->

	<!-- footer -->
	<div class="footer-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="footer-box about-widget">
						<h2 class="widget-title">About us</h2>
						<p>Baristavro is a website aimed at coffee lovers, baristavro also sells a variety of tools and coffee beans to help you in perfecting your coffee worship.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="footer-box pages">
						<h2 class="widget-title">Contact Us</h2>
						<ul>
							<li>Email</li>
							<li>Call us</li>
							<li>Chat online</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="footer-box get-in-touch">
						<h2 class="widget-title">Policy</h2>
						<ul>
							<li>Shipping</li>
							<li>Returns and Refund</li>
							<li>Privacy Policy</a></li>
							<li>Cookie Policy</li>
						</ul>
					</div>
				</div>
				<!-- logo -->
				<div class="site-logo">
					<a href="index.html">
						<img src="assets/img/logo.png" alt="">
					</a>
				</div>
				<!-- logo -->
			</div>
		</div>
	</div>
	<!-- end footer -->
	
	<!-- copyright -->
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-12">
					<p>Copyrights &copy; 2019 - <a href="index.html">Baristavro</a>,  All Rights Reserved.</p>
				</div>
				<div class="col-lg-6 text-right col-md-12">
					<div class="social-icons">
						<ul>
							<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end copyright -->
	
	<!-- jquery -->
	<script src="{{url ('') }} /assets/js/jquery-1.11.3.min.js"></script>
	<!-- bootstrap -->
	<script src="{{url ('') }} /assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- count down -->
	<script src="{{url ('') }} /assets/js/jquery.countdown.js"></script>
	<!-- isotope -->
	<script src="{{url ('') }} /assets/js/jquery.isotope-3.0.6.min.js"></script>
	<!-- waypoints -->
	<script src="{{url ('') }} /assets/js/waypoints.js"></script>
	<!-- owl carousel -->
	<script src="{{url ('') }} /assets/js/owl.carousel.min.js"></script>
	<!-- magnific popup -->
	<script src="{{url ('') }} /assets/js/jquery.magnific-popup.min.js"></script>
	<!-- mean menu -->
	<script src="{{url ('') }} /assets/js/jquery.meanmenu.min.js"></script>
	<!-- sticker js -->
	<script src="{{url ('') }} /assets/js/sticker.js"></script>
	<!-- form validation js -->
	<script src="{{url ('') }} /assets/js/form-validate.js"></script>
	<!-- main js -->
	<script src="{{url ('') }} /assets/js/main.js"></script>
	
</body>
</html>
